// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import VueX from 'vuex'
import AppHome from './components/AppHome'
import AppFormAddGame from './components/AppFormAddGame'
import AppFormStartSeason from './components/AppFormStartSeason'
import AppLogin from './components/AppLogin'
import AppSeasonList from './components/AppSeasonList'
import AppPlayers from './components/AppPlayers'
import AppAdmin from './components/AppAdmin'

Vue.use(VueRouter)
Vue.use(VueX)

const routes = [
  { path: '/home', component: AppHome },
  { path: '/seasons', component: AppSeasonList },
  { path: '/login', component: AppLogin, meta: {Auth: true} },
  { path: '/addseason', component: AppFormStartSeason },
  { path: '/addgame/:seasonId', component: AppFormAddGame, props: true },
  { path: '/seasonlist', component: AppSeasonList },
  { path: '/players', component: AppPlayers },
  { path: '/admin', component: AppAdmin },
  { path: '/logout' }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (store.state.user.authenticated && to.path === '/login') {
    next({ path: '/home' })
  } else if (!store.state.user.authenticated && !to.meta.Auth) {
    next({ path: '/login' })
  } else if (to.path === '/logout') {
    store.commit('authenticated', false)
    store.commit('adminRole', false)
    next({ path: '/login' })
  } else if (to.path === '/') {
    next({ path: '/home' })
  } else {
    next()
  }
})

const store = new VueX.Store({
  state: {
    api: 'http://cs-api.bowling-results.com/',
    seasons: [],
    lookup: {},
    user: {
      authenticated: false,
      admin: false
    }
  },
  mutations: {
    addSeasons (state, seasons) {
      state.seasons = []
      for (var column in seasons) {
        seasons[column].id = column
        state.seasons.push(seasons[column])
      }
      state.seasons.reverse()
      for (var i = 0, len = state.seasons.length; i < len; i++) {
        state.lookup[state.seasons[i].id] = state.seasons[i]
      }
    },
    authenticated (state, loggedIn) {
      state.user.authenticated = loggedIn
    },
    adminRole (state, adminRole) {
      state.user.admin = adminRole
    }
  }
})

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  store,
  template: '<App/>',
  components: { App }
})
